#! /bin/bash

here="`dirname \"$0\"`"
cd "$here/scss" || exit 1

sass --watch style.scss:../css/style.min.css --style compressed
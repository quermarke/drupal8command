#! /bin/bash

# for (( i = 0; i < 17; i++ )); do echo "$(tput setaf $i)This is ($i) $(tput sgr0)"; done

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "Please enter your path"
echo -e "[$(tput setaf 2) $CURRENT_DIR $(tput sgr0)]"

read INPUT_DIR

DIR=${INPUT_DIR:-$CURRENT_DIR}

cd "$DIR"

echo -e "Please enter your folder name: "
echo -e "Will Create in [$(tput setaf 2) $DIR $(tput sgr0)]"

read INPUT_FOLDER

drupal site:new "$INPUT_FOLDER"

cd "$INPUT_FOLDER"

drupal site:install standard --langcode de --db-type mysql --db-host 127.0.0.1 --db-user root --db-pass root --db-port 8889 --account-name quermarke --account-mail info@quermarke.eu
drupal module:download image_effects focal_point examples --latest
drupal module:download field_group inline_entity_form google_analytics --latest
drupal module:install entity_reference_revisions pathauto ctools token admin_toolbar devel paragraphs --latest
drupal theme:download bootstrap


# ==========================
# SUBTHEME
# copy the less subtheme, rename and make a sass theme



echo -e "Please enter your subtheme $(tput setaf 1)machine name$(tput sgr0)"
read INPUT_SUBTHEME_MACHINE_NAME

INPUT_SUBTHEME_MACHINE_NAME="bootstrap_$INPUT_SUBTHEME_MACHINE_NAME"

echo -e "Please enter your subtheme title"
read INPUT_SUBTHEME_TITLE

echo -e "Please enter your subtheme description"
read INPUT_SUBTHEME_DESCRIPTION

DIR_SUBTHEME="$DIR/$INPUT_FOLDER/themes/$INPUT_SUBTHEME_MACHINE_NAME"



# SUBTHEME Rename

cp -av "$DIR/$INPUT_FOLDER/themes/bootstrap/starterkits/less" "$DIR_SUBTHEME"

# move
mv "$DIR_SUBTHEME/THEMENAME.libraries.yml" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.libraries.yml"
mv "$DIR_SUBTHEME/THEMENAME.starterkit.yml" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.info.yml"
mv "$DIR_SUBTHEME/THEMENAME.theme" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.theme"

mv "$DIR_SUBTHEME/config/install/THEMENAME.settings.yml" "$DIR_SUBTHEME/config/install/$INPUT_SUBTHEME_MACHINE_NAME.settings.yml"
mv "$DIR_SUBTHEME/config/schema/THEMENAME.schema.yml" "$DIR_SUBTHEME/config/schema/$INPUT_SUBTHEME_MACHINE_NAME.schema.yml"

# delete
rm -rf "$DIR_SUBTHEME/less"
rm -rf "$DIR_SUBTHEME/css"

# rename
sed -i '' "s/Bootstrap Sub-Theme (LESS)/$INPUT_SUBTHEME_TITLE/g" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.info.yml"
sed -i '' "s/Uses the Bootstrap framework LESS source files and must be compiled (not for beginners)./$INPUT_SUBTHEME_DESCRIPTION/g" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.info.yml"

sed -i '' "s/THEMENAME/$INPUT_SUBTHEME_MACHINE_NAME/g" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.info.yml"
sed -i '' "s/THEMENAME/$INPUT_SUBTHEME_MACHINE_NAME/g" "$DIR_SUBTHEME/config/schema/$INPUT_SUBTHEME_MACHINE_NAME.schema.yml"
sed -i '' "s/THEMETITLE/$INPUT_SUBTHEME_TITLE/g" "$DIR_SUBTHEME/config/schema/$INPUT_SUBTHEME_MACHINE_NAME.schema.yml"

sed -i '' "s/css\/style.css/styles\/css\/style.min.css/g" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.libraries.yml"
sed -i '' "s/bootstrap\/js/js\/bootstrap\/components/g" "$DIR_SUBTHEME/$INPUT_SUBTHEME_MACHINE_NAME.libraries.yml"








# load the master brunch and get file and folder name
# if it is a private: http://stackoverflow.com/questions/17682143/download-private-bitbucket-repository-zip-file-using-http-authentication
URL="https://bitbucket.org/quermarke/drupal8command/get/master.zip"
FILENAME=$(curl -sI  $URL | grep -o -E 'filename=.*$' | sed -e 's/filename=//')
curl -o $FILENAME -L $URL
# delete .zip extension / last .*
FOLDERNAME=${FILENAME%.*}

unzip $FILENAME -d "$DIR_SUBTHEME"

# echo $(tput setaf 4)"$DIR_SUBTHEME/$FOLDERNAME/"$(tput sgr0)
# move hidden files like .gitignore
shopt -s dotglob nullglob
#move all files and folders to dir
mv "$DIR_SUBTHEME/$FOLDERNAME/"* "$DIR_SUBTHEME/"

# delete temp folder and zip
rm -rf "$DIR_SUBTHEME/$FOLDERNAME/"
#rm -f "$DIR_SUBTHEME/$FILENAME"
rm -f "$FILENAME"
rm -f "$DIR_SUBTHEME/create8.command"



#Install theme, set default and unistall unused theme

drupal theme:install "$INPUT_SUBTHEME_MACHINE_NAME" --set-default
drupal theme:uninstall bartik 